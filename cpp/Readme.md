## [C++里怎么获得数组的长度](https://stackoverflow.com/questions/4108313/how-do-i-find-the-length-of-an-array)
- `(sizeof(a)/sizeof(*a))`

## [C++里怎么方便地遍历文件夹](https://stackoverflow.com/a/612176/4291968)
- `#include <dirent.h>`

## [C++里怎么加载tiff图像](https://stackoverflow.com/a/54315899/4291968)
- 用这个库`libtiff`
- 然后参考上面这个教程，可以成功编译
- 真正用到的库是`tiff.lib`这一个

## [vs的cmd不要自动退出](https://blog.csdn.net/youmingyu/article/details/53467913)
- 在链接器里把系统设置为控制台

## [类里的static的map变量在链接时报LNK2001错误](https://stackoverflow.com/a/3585104/4291968)
- 静态变量需要初始化！！！
- cpp文件里加上初始化语句！！！

## [C++里判断文件是否存在](https://stackoverflow.com/a/15471417/4291968)
- 用 open file

## [LNK4098: 链接库冲突](https://blog.csdn.net/wwfish/article/details/42917105)
- 一般出现在从debug切换到release，然后没有把对应lib文件里的debug文件删除改成release文件的时候
- 一般d结尾的是debug的库
- 如果报的错里的冲突的库是debug的库，在alt+enter-链接器-忽略特定默认库里把这个库的名字加上（查一下对应哪个lib文件，然后把该lib文件名填到那一栏）