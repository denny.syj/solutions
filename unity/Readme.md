## 如果在当前Component的脚本中获取parent的GameObject？
- `GetComponent<CurrentType>().gameObject.transform.parent.gameObject`

## [Destroy函数调用后对象没有立刻删除](http://answers.unity.com/answers/13842/view.html)
- 因为`==`被重载了，判定是否为null可以得到true，但是实际上引用还能调用

## [Unity停止自动编译](https://blog.csdn.net/Teddy_k/article/details/90726612)
- Edit -> Preferences -> General

## [Unity的RenderTexture](https://zhuanlan.zhihu.com/p/22447578)
- Bug很多