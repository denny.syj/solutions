# Ubuntu
## 输入法
- `googlepinyin` [Install google pinyin in Ubuntu 16.04](https://hhddkk.wordpress.com/2016/06/02/install-google-pinyin-in-ubuntu-16-04/)
- `sougoupinyin` 这玩意还是算了吧

## 定时任务
- `crontab`[ubuntu设置自己的定时任务](https://askubuntu.com/questions/2368/how-do-i-set-up-a-cron-job), [crontab命令查询](https://crontab.guru/#0_12_*_*_*)

## 依赖
- `libstdc++.so.6: cannot open shared object file: No such file or directory`[libstdc++6](https://stackoverflow.com/questions/11471722/libstdc-so-6-cannot-open-shared-object-file-no-such-file-or-directory)

## 解压7z
- [`p7zip-full`](https://askubuntu.com/questions/219392/how-can-i-uncompress-a-7z-file)

## 刷新Dns
- `sudo /etc/init.d/networking restart`

## 查看本机字体
- `fc-list :lang=zh`

## [给apt配置代理](https://askubuntu.com/a/920242/870630)
- 命令行的代理对于apt来说是没有用的！！！
- 必须给apt配置代理才行！！！

## ssh
- [修改`ssh-server`的默认连接端口](https://sg.godaddy.com/zh/help/linux-ssh-7306?lang=en)
- 在默认是`fish-shell`的机器上, `ssh-copy-id`执行失败, 貌似还没被解决, 所以需要使用[手动拷贝](https://unix.stackexchange.com/questions/29386/how-do-you-copy-the-public-key-to-a-ssh-server)
- 手动拷贝完public key, 仍然无法使用key来登录, [需要检查`authorized_keys`文件的权限(至少为`700`)](https://unix.stackexchange.com/questions/36540/why-am-i-still-getting-a-password-prompt-with-ssh-with-public-key-authentication)
- 完成端口修改之后, 禁止密码登录, 只允许key登录, 只允许非Root用户登录等[ssh的权限配置操作](https://askubuntu.com/questions/894237/disabling-ssh-password-login-for-ubuntu)

## 列出相近的命令
- [当命令输入错误时, 只展示列出的相近命令的条数但是没有展示是哪些](https://askubuntu.com/questions/197633/any-way-to-list-similar-commands)

## 退出命令行但不关闭进程
- <kbd>Ctrl+Z</kbd>
- `bg`
- `disown`
- `exit`

## [根据正则表达式删除文件](https://superuser.com/a/392878/1045842)
```
find ./ | grep -P '\(1\)' | xargs -d "\n" rm
```

## [提取pdf的目录](https://stackoverflow.com/a/37073490/4291968)
- `mutool show my.pdf outline`

## [把多张图片和文件合并为一个pdf文件](https://askubuntu.com/a/473674/870630)
- `convert image1.jpg image2.png text.txt PDFfile.pdf outputFileName.pdf`

## [VirtualBox - RTR3InitEx failed with rc=-1912 (rc=-1912)](https://askubuntu.com/a/902991/870630)
- 就是重装
- 重装之后会遇到extend pack没装导致的虚拟机无法启动问题，强制清除开机状态，然后把usb设置去掉就行了

## [设置fish为默认shell](https://askubuntu.com/a/26440/870630)
```
chsh -s which `fish`
```

## [Nginx配置SSL](https://yq.aliyun.com/articles/549355?spm=5176.10695662.1996646101.searchclickresult.315d2c684hTwGS)
- 那个教程其实写得不怎么样，跟现在的nginx版本已经不对应了，在已经按照`Ocean`的教程让网站成功跑起来之后，按照以下步骤完成Nginx的SSL配置
1. 去阿里云申请免费证书，然后解压分别获得pem文件和key文件
2. 将上述两个文件上传到服务器，然后创建/etc/nginx/cert目录（/etc/nginx就是nginx的安装目录）
3. 进入sites-available目录，找到网站的配置文件，在原有的server条目下完善如下SSL配置信息
```
#       listen 80;  # 先把原来的http端口停掉
        listen 443; # 改为https端口
        server_name <你申请的域名>;

        ssl on;
        ssl_certificate cert/<你的pem文件名>.pem;
        ssl_certificate_key cert/<你的key文件名>.key;
        ssl_session_timeout 5m;
        ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:ECDHE:ECDH:AES:HIGH:!NULL:!aNULL:!MD5:!ADH:!RC4;
        ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
        ssl_prefer_server_ciphers on;
```
4. 如果需要让http重定向到https，那么就在上述的配置文件中添加一条并列的server配置
```
server {
        listen 80;
        server_name <你的域名>;
        return  301 https://$server_name$request_uri;
}
```
5. 然后测试一下配置文件`sudo nginx -t`，如果成功，就重载配置文件`sudo nginx -s reload`，配置完成

<!-- ## [威二射线的安装](https://www.v2ray.com/chapter_00/install.html)
```
bash <(curl -L -s https://install.direct/go.sh)
# 然后复制粘贴这台电脑的config.json
```
-->

## [查看当前登录用户]()
- `whoami`