# 数据库-MySQL-JDBC
## [把`.sql`文件导入数据库](https://stackoverflow.com/questions/17666249/how-to-import-an-sql-file-using-the-command-line-in-mysql)

## [数据库的远程登录](https://stackoverflow.com/questions/15663001/remote-connections-mysql-ubuntu)

## [MySQL远程登录时的Error1130](https://stackoverflow.com/questions/2857446/error-1130-in-mysql)

## 当表不存在时创建: `create table if not exists tablename (columns);`

## [获取table的row count](https://stackoverflow.com/questions/162571/how-do-i-get-the-row-count-in-jdbc)
```
ResultSet rs = st.executeQuery("select count(*) from TABLE_NAME");
rs.next();
int count = rs.getInt(1);
```

## [JDBC连接获得的utf8字符变成`?????`](https://stackoverflow.com/questions/3040597/jdbc-character-encoding)
- 需要在连接的时候指定使用utf8字符集

## [保存hash的最好数据类型](https://stackoverflow.com/questions/2326584/best-practices-for-efficiently-storing-md5-hashes-in-mysql)
- `char(32)`

## [限制column唯一](https://www.w3schools.com/sql/sql_unique.asp)
- `create table x(id int not null, unique (id));`

## [让id递增](https://stackoverflow.com/questions/17893988/how-to-make-mysql-table-primary-key-auto-increment-with-some-prefix)
- `create table x(id int not null auto_increment primary key);`

## [python connector 插入数据](https://www.w3schools.com/python/python_mysql_insert.asp)

## [插入重复数据](https://www.tutorialspoint.com/mysql/mysql-handling-duplicates.htm)
- `insert ignore into table...`
