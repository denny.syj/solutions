# 小百合
- [blog的UBB代码](http://bbs.nju.edu.cn/vd10588/bbstcon?board=PopMusic&file=M.1148995313.A)
- [UBB代码](https://baike.baidu.com/item/UBB%E4%BB%A3%E7%A0%81/3048502?fr=aladdin)

# vbox
## 共享文件夹(host:Ubuntu, guest:win7)
1. 在`设备-共享文件夹`菜单直接编辑
2. 重启guest系统

## [VirtualBox: Error -610 in supR3HardenedMainInitRuntime!](https://askubuntu.com/a/993505/870630)
- vbox出现奇怪的错误（突然打不开）
```
VirtualBox: Error -610 in supR3HardenedMainInitRuntime!
VirtualBox: dlopen("/usr/lib/virtualbox/VBoxRT.so",) failed: <NULL>

VirtualBox: Tip! It may help to reinstall VirtualBox.
```
- 原因是`/usr/lib`的owner需要设置为`root:root`并且它的权限需要设置为`755`
- 因为`777`的话所有人都可以修改这个位置的内容，很危险（大概是这个意思）

## [安装的安卓系统没法用鼠标](https://android.stackexchange.com/a/181467)
- 在`设置-系统`里把触控板改成鼠标

## [安装的游戏跑不起来]
- 首先需要开启[arm支持](https://stackoverflow.com/a/49684842/4291968)
- 在开启arm支持的时候，在enable-nativebridge这一步，会出现因为墙和url重定向而运行失败的问题，解决方法是在host上把sfs文件下载下来，然后用adb push保存到对应目录

# svn
## [使用svn进行clone的命令](https://stackoverflow.com/questions/19477327/cloning-a-subversion-project-from-google-code-using-git)
```
svn co url
```

# Eclipse
## [`Eclipse Build Error: A class file was not written. The project may be inconsistent, if so try refreshing this project and building it`](https://stackoverflow.com/questions/17154360/eclipse-build-error-a-class-file-was-not-written-the-project-may-be-inconsiste/53743805#53743805)
- 把bin文件夹删除，然后进Eclipse运行`Project->Clean...`然后就好了

# Git
## [如何获取最新一次提交的hash](https://stackoverflow.com/questions/949314/how-to-retrieve-the-hash-for-the-current-commit-in-git)
`git log -1 --format='%H'`

## [如何查看remote的url](https://help.github.com/articles/changing-a-remote-s-url/)
- `git remote -v`


# tex
## [当图片超出文本宽度的时候怎么居中](https://tex.stackexchange.com/a/27099/173445)
- `centerfloat`
```
\makeatletter
\newcommand*{\centerfloat}{%
  \parindent \z@
  \leftskip \z@ \@plus 1fil \@minus \textwidth
  \rightskip\leftskip
  \parfillskip \z@skip}
\makeatother
```

## [删除目录超链接的方框](https://tex.stackexchange.com/a/155479/173445)
```
\begingroup
  \hypersetup{hidelinks}
  \tableofcontents
\endgroup
```

## [目录加点](https://tex.stackexchange.com/a/7352/173445)
```
\usepackage{tocloft}
\renewcommand{\cftsecleader}{\cftdotfill{\cftdotsep}}
```

## [tex的目录分成2列](https://tex.stackexchange.com/a/24344/173445)
```
\begin{multicols}{2}
  \tableofcontents
\end{multicols}
```

## [tex的目录中间空格增大]()
```
\setlength{\columnsep}{1cm}
```

## [PDF编译的时候提示浮动图片太多 Float ](http://mirrors-wan.geekpie.club/CTAN/macros/latex/contrib/placeins/placeins.txt)
```
# 在每一页末尾插一个
\FloatBarrier
```

## [横版A4](https://tex.stackexchange.com/questions/147504/how-to-make-a-latex-document-in-landscape/147507#147507)
```
\documentclass[a4paper,landscape]{article}
```

## [Section Name居中](https://tex.stackexchange.com/questions/107281/centering-chapter-section-subsection)
```
\usepackage[center]{titlesec}
```

## [删除texlive](https://tex.stackexchange.com/questions/95483/how-to-remove-everything-related-to-tex-live-for-fresh-install-on-ubuntu)
- 很多操作一步步来

# FBX SDK
## [编译fbx的sample的时候报错`/usr/bin/ld: 找不到 -luuid`]
- 安装包`uuid-dev`(`sudo apt install uuid-dev`)
- 在编译的时候设置为`x64` (`make -e M64=-m64`)

# PDF
## [PDF文件压缩图片](https://askubuntu.com/a/256449/870630)
```
gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/screen -dNOPAUSE -dQUIET -dBATCH -sOutputFile=output.pdf input.pdf
```

# Windows
## [Windows下设置alias](https://stackoverflow.com/a/39459404/4291968)

## [Windows下Cmake找不到VS实例](https://stackoverflow.com/a/52530486/4291968)

## [Windows下VS的C4819警告导致的明明代码都写对但是找不到符号的问题](https://blog.csdn.net/yuliying/article/details/61916471)

## [Windows下VS无法启动调试的问题：`(Win32): 已加载“C:\Windows\System32\ntdll.dll”。无法查找或打开 PDB 文件。`](https://blog.csdn.net/sunnyliqian/article/details/50273987)

## [adobe acrobat reader改变背景颜色](https://zhidao.baidu.com/question/55196774.html)
- `编辑-首选项-辅助工具-替换文档颜色`

## [Windows下vs2019修改“1个引用”的字体]
- CodeLens字体