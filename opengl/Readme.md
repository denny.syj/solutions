# OpenGl
## [The Xinerama/Xcursor library and headers were not found](https://askubuntu.com/a/642719/870630)
```
sudo apt-get install libxinerama-dev
sudo apt-get install libxcursor-dev
```

## [x11链接错误]()
```
# 卸载libxi-dev，然后删除所有文件重新build
sudo apt-get remove libxi-dev
```

## Tutorial第二课里找不到shader文件后卡屏
- 在QtCreator的Projects的Run选项里，把working directory设置为playground所在目录
- 在cmakelists.txt的playground项目的add_executable配置里，加上两个shader（这样qtcreator就会把它们显示在目录树里）

## Tutorial第二课里shader配置正确却无法正常显示三角形
- 没有在`glewInit()`之前设置`glewExperimental = true;`
- 这个选项大意是允许GLEW获取试用版本或者实验版本的驱动明明可以支持,但是因为某些原因不出现在驱动的支持列表里的扩展名,[参考资料](https://blog.csdn.net/xiaoxiaoyusheng2012/article/details/43992445)
- 在win下,如果不设置选项就会出现内存非法访问错误

## [GLIBCXX_3.4.22 not found](https://stackoverflow.com/a/46613765/4291968)
- 没有安装合适的库
- 按照这个里的命令进行安装

## **不知道为什么，用compressonatorCLI压缩得到的dds没法正常显示**
- 换`stb_image`库

## 模型的光线来源
- 环境光本身（直接让模型本身发光，只考虑颜色）
- 环境光的反射光（距离、角度和颜色，也就是所谓的高光）
- 镜面光：来自光源的反射光（考虑光源的距离、颜色、反射角度）

## 第11课的时候，两个模型，不知道为什么文字倒计时还在猴子不见了
- 忘记在主循环里用`glUseProgram(programID);`来加载shader了，愚蠢

## 第11课不知道为什么没法打开alpha通道的透明效果
- **TODO**

## 第13课的TBN矩阵中TB的计算，为什么这样列方程？
- 我感觉可以这样理解：
  - 三角形原来的两条边分别是`a`和`b`，放到法线空间，那么`dpos1 = uv1 - a`，`dpos2 = uv2 - b`，因为`uv1, uv2, a, b`都是已知的，所以可以直接算出`dpos1，dpos2`
  - 但是用两个奇奇怪怪的向量相减太奇怪了，所以我们现在有了这两个法线平面的方向（根据纹理可得），那么我们就可以把他们作为坐标轴，接下来只要获得一个对应的`单位向量`（大佬说的维度向量）TB，就可以直接像用笛卡尔坐标系一样来在法线空间中表示`dpos1和dpos2`，所以列出了这个式子。
- 所谓的切线空间，应该就是指纹理的空间

## 第14课时`:1(1): error: syntax error, unexpected $end`
- 太蠢了，这个错误的原因之一是shader的名称品错（single和simple写混了）
- 调了几个小时吧，靠
- 后面的小作业没有做

## 第15课`bake的时候报找不到object的错`
- 好像是bake的时候需要把光源也选中
- 我也不懂啊靠

## 第16课`图案被谜样扭曲，有各种奇怪的三角形`
- 检查buffer里data装得是否对应（indexed之后，仍然把未indexed的data装入buffer会出现这种不对应的情况）

## OpenGL的库之间的关系
- OpenGL本体：它是一个用于进行2D/3D图形渲染的跨平台函数库**的API！（真正的每一个函数的实现在GPU的驱动代码里）**
- OpenGL Extensions：现在GPU非常强大，所有GPU厂商都实现了OpenGL的API，还有的GPU会针对某些图形渲染函数进行优化，或者干脆提供新的渲染函数，这些函数集合成库就是OpenGL Extensions。用户通过在OpenGL里注册Extensions就可以使用Extensions了。
    - 常用的OpenGL Extension加载库有GLEW，GLEE，glbinding
- 要让OpenGL的渲染函数跑起来，就需要创建一个允许画图的窗口，而这个窗口的创建、控制、操作等操作如何与原有系统一一对应，其实是一件很复杂的事，所以开发者们把这些窗口操作、IO控制操作封装为库。
    - 现有的窗口控制、IO控制库主要有：GLFW、freeglut、GLUT
    - 类似的还有支持多媒体的库：Allegro 5、SDL、SFML等
    - 还有Widget工具：Qt、FLTK、wxWidget

> 另外，OpenGL里涉及了很多数学操作，所有的这些操作都被封装在GLM里

- **在所有以上的库里，推荐的组合是GLFW+GLEW**

# Irrlicht
## 关于如何让Irrlicht的Examples跑起来
- 解压文件夹，进入`source/Irrlicht`目录，然后`make`，这样就能编译得到动态链接库（在`lib`文件夹里）
- 然后才能进example文件夹编译例子

# FBX SDK
## [关于使用cmake生成vs项目后，进行全部生成报LNK1104，库路径错误的问题]()
- 目前的解决方式是打开`vcxproj`文件，然后找到对应的路径，修改为正确路径，目前还不知道为什么会出现这个错误
- 正确的解决方式是打开sample文件夹里的cmakelist.txt文件，在里面最前面加一行`SET(FBX_COMPILER "vs2017")`（这边的vs2017是根据lib文件夹里的目录名字来设定的，根据自己的目录名字进行设定就行）
## [C2440: static_cast 无法从const A转换为T type vec2.inl]()
- 这个问题报error但是却没有显示代码行，很难定位
- **其实真正的原因是使用glm::vec2，进行初始化的时候，其初始化类型不符导致的**

# LearnOpenGL
## [着色器一节中的glm找不到文件问题]()
- 去github上下glm的源代码，然后添加到include目录里去
## [C7548]()
```
ERROR::SHADER_COMPILATION_ERROR of type: FRAGMENT
0(2) : error C7548: 'layout(location)' requires "#extension GL_ARB_separate_shader_objects : enable" before use
```
- 一般是vs和fs的顺序搞错了！把vs当fs编译就会遇到这个问题