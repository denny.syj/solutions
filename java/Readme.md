# Java
## 生成范围内随机数
```
ThreadLocalRandom.current().nextInt(0, 10)
```

## Maven
- [阿里的Maven仓库](https://yq.aliyun.com/articles/621196?spm=a2c40.aliyun_maven_repo.0.0.dc983054RQuJUB)

## 命令行参数的处理
- [argparse4j](https://argparse4j.github.io/)

## [Java程序中调用其他命令的ProcessBuilder实现](https://stackoverflow.com/a/41292766/4291968)
```
private String executeCmd(String[] cmd, String dir) {
  ProcessBuilder pb = new ProcessBuilder(cmd);
  pb.directory(new File(dir));
  String result = "";
  try {
    Process p = pb.start();
    final BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

        StringJoiner sj = new StringJoiner(System.getProperty("line.separator"));
        reader.lines().iterator().forEachRemaining(sj::add);
        result = sj.toString();
        p.waitFor();
        p.destroy();
  } catch (IOException | InterruptedException e1) {
    e1.printStackTrace();
  }

  return result;
}
```

## [Java中byte数组转String](https://www.mkyong.com/java/how-do-convert-byte-array-to-string-in-java/)
```
// 必须使用new，最好加上编码
byte[] tmp = ...;
String tmp_str = new String(tmp, "UTF-8");
```

## [Java GUI中Jtable如何根据cell的内容改变行的颜色](https://stackoverflow.com/questions/3875607/change-the-background-color-of-a-row-in-a-jtable)
```
// 改变单个单元格的颜色貌似会更困难一些，但行的颜色可以这样设置
// 这是SO上经过本地修改的版本，需要注意NPE错误
regTable.setDefaultRenderer(Object.class, new DefaultTableCellRenderer()
		{
			private static final long serialVersionUID = 2959137775367628479L;

			@Override
		    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
		    {
		        final Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		        if (column == 3) {
		        	Object val = table.getValueAt(row, column);
		        	if (val != null) {
				        String res = val.toString();
				        if (res.equalsIgnoreCase("safe")) {
				        	c.setBackground(Color.GREEN);
				        } else if (res.equalsIgnoreCase("dangerous")) {
				        	c.setBackground(Color.RED);
				        }
		        	}
		        }
		        return c;
		    }
		});
```

## [Java里读取Json文件](https://stackoverflow.com/a/18998203/4291968)

## [Java里把List<String>连接成String](https://stackoverflow.com/a/22577565/4291968)

## [Jar包的混淆配置](https://blog.csdn.net/shensky711/article/details/52770993)
- 见文件`config.file`

## [Maven打包的时候去掉不想打包的类]()
- 注意那个exclude的路径，它实际上是按照target文件的层次顺序来罗列，而不是根据src的顺序来的（当然eclipse的默认设置是两者结构相同，但是难保不一样）
```
<build>
  <sourceDirectory>src</sourceDirectory>
  <plugins>
    <plugin>
      <artifactId>maven-compiler-plugin</artifactId>
      <version>3.7.0</version>
      <configuration>
        <source>1.8</source>
        <target>1.8</target>

        <excludes>
          <exclude>cn/edu/nju/moon/test/*</exclude>
          <exclude>cn/edu/nju/moon/datautils/Draw*</exclude>
          <exclude>cn/edu/nju/moon/datautils/imp/DB*</exclude>
          <exclude>cn/edu/nju/moon/strategy/imp/BruteForce*</exclude>
        </excludes>
      </configuration>
    </plugin>
  </plugins>
</build>
```

## [Java里的JProfiler的注册](https://download.csdn.net/download/wupiaobei5924/11068355)
- 11版本，使用注册机（username那边随便输）

## [Maven安装和Eclipse的Maven切换](https://stackoverflow.com/a/25468277/4291968)

## [关于打包、混淆和使用的一点说明]()
- 打包：已经配置好pom.xml文件，提供2种打包方式
	- 方式1：使用eclipse自带的maven插件，右键工程名然后`Run As->Maven Clean`然后`Run As->Maven install`）
	- 方式2：使用系统的maven（要求系统安装有maven，测试方式：命令行敲mvn -version有输出），进入工程目录，然后运行`mvn clean install`
	- 以上两种方式都会在target目录新生成一个Highway-版本号.jar的包
- 混淆：要求系统有ProGuard
	- 运行图形界面
	- `load configurations`（加载项目根目录的obf-config.file）
	- 在Input/Output界面重新指定输入输出（**必须**）
	- 到Process界面点击`Process!`
	- 然后就可以获得一个混淆后的jar包
- 使用：要求会使用maven
	- 在Eclipse下手动导入到现有项目
		- 在现有项目下，点击File->Import->Install or deploy an artifact to a maven repository弹出信息填写框
		- 第一项点击浏览，找到Jar包
		- 第二项点击浏览，找到当前项目的pom文件，打包方式为jar，其他必要信息如下
```
<groupId>cn.edu.nju.moon</groupId>
<artifactId>Highway</artifactId>
<version>0.0.1</version>
```
	- [使用命令行导入（推荐）](https://www.cnblogs.com/hym-pcitc/p/5632468.html)
		- 先把Jar包安装到本地仓库`mvn install:install-file -Dfile=<jar包的路径> -DgroupId=cn.edu.nju.moon -DartifactId=Highway -Dversion=0.0.1 -Dpackaging=jar`
		- 然后打开项目文件，把以下代码加入到<dependencies>中
```
<dependency>
  <groupId>cn.edu.nju.moon</groupId>
  <artifactId>Highway</artifactId>
  <version>0.0.1</version>
</dependency>
```
- 其他：[卸载Jar包](https://blog.csdn.net/nianbingsihan/article/details/87191076)
	- `mvn dependency:purge-local-repository -DmanualInclude="cn.edu.nju.moon:Highway"`
	- `mvn dependency:purge-local-repository -DreResolve=false`]
