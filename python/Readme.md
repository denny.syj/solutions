# Python3
## 功能
- 获取html页面: [requests](), [urllib]()

## 包
### requests
- `get`乱码: [解决Requests中文乱码](https://blog.csdn.net/chaowanghn/article/details/54889835)
- `requests`中文乱码终极解决方案 [UnicodeDecodeError ：'gb2312' codec can't decode bytes：illegal multibyte sequence](https://blog.csdn.net/Junkichan/article/details/51913845)

## 环境
### python3.7环境
- [在`ubuntu14.04/16.04`下配置`python3.7`虚拟环境](https://askubuntu.com/questions/865554/how-do-i-install-python-3-6-using-apt-get#)

### datetime
- `yesterday`昨天: [format yesterday](https://stackoverflow.com/questions/1712116/formatting-yesterdays-date-in-python)

## [byte转str](https://stackoverflow.com/questions/606191/convert-bytes-to-a-string)
- `bytes.decode('utf8')`

## [导入另一个python文件中的函数](https://stackoverflow.com/questions/2349991/how-to-import-other-python-files)
- 把那个文件放入文件夹, 然后在文件夹下创建`__init__.py`文件把它变成一个包, 然后使用`from dirname.filename import functionname`导入
- [使用importlib](https://stackoverflow.com/questions/8790003/dynamically-import-a-method-in-a-file-from-a-string)

## [计算字符串的md5](https://stackoverflow.com/questions/5297448/how-to-get-md5-sum-of-a-string-using-python)
- `hashlib.md5("str".encode('utf8')).hexdigest()`

## [安全创建目录](https://stackoverflow.com/questions/273192/how-can-i-safely-create-a-nested-directory-in-python)
- `os.path.exists()`
- `os.makedirs()`

## [Python的命令行参数处理](https://stackoverflow.com/questions/52403065/argparse-optional-boolean)
- `argparse`
- `parser.add_argument('-c', '--collect', default = False, action = 'store_true', help = 'Current operation is collect.')`

## [Python获取图片的长和宽](https://stackoverflow.com/questions/15800704/get-image-size-without-loading-image-into-memory)
```
width, height = imagesize.get('xx.png')
```
